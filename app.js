// account information
const userAccount = {
    bankBalance: 0,
    receivedLoan: false,
    salaryRate: 100,
    outstandingPay: 0
};

// laptops 
const laptopList = [
    {
        name: "Lenovo ThinkPad T490s",
        price: 1595,
        features: ["8GB Ram", "Up to 20 hours battery life", "Webcam"],
        description: "Great work laptop",
        imageurl: ""
    },
    {
        name: "HP 14DQ",
        price: 595,
        features: ["Lightweight", "Fingerprint sensor"],
        description: "A good school laptop",
        imageurl: ""
    },
    {
        name: "Asus VivoBook",
        price: 395,
        features: ["4GB", "Webcam"],
        description: "A starter laptop",
        imageurl: ""
    }
]


// display laptops in dropdown menu
const dropdown = document.getElementById("laptop-dropdown-selection");
laptopList.forEach(laptop => {
    const newElem = document.createElement("option");
    newElem.setAttribute("value", laptop.name);
    newElem.innerText = laptop.name;
    dropdown.appendChild(newElem);
})

// get rest of dom elements
// buttons:
const loanBtn = document.getElementById("loan-btn");
const bankBtn = document.getElementById("bank-btn");
const workBtn = document.getElementById("work-btn");
const buyButton = document.getElementById("buy-btn");


// display areas
const balanceDisplay = document.getElementById("balance-display");
const payDisplay = document.getElementById("pay-display");
const laptopFeaturesParagraph = document.getElementById("features");
const laptopInfoDisplay = document.getElementById("laptop-info-section");


// add event listeners
loanBtn.addEventListener("click", getLoan);
bankBtn.addEventListener("click", addToBank);
workBtn.addEventListener("click", doWork);
buyButton.addEventListener("click", buyLaptop);
dropdown.addEventListener("change", displayLaptopInfo);


// functions

// checks if eligible for loan, takes in amount and updates balance
function getLoan(event) {
    // check if eligible for a loan
    if (userAccount.receivedLoan) {
        alert("You have already received a loan");
        return;
    }
    // show promt - enter value
    let returnValue = prompt("Enter loan amount: ");
    if (!returnValue) {
        // user clicked cancel
        return;
    }
    // convert to number
    let amount = Number(returnValue);
    
    // check valid input
    if (isNaN(amount)) {
        alert("Please enter a number");
        return;
    }

    // check if user is approved for loan
    if (amount > userAccount.bankBalance*2) {
        alert("You do not have a high enough bank balance to get this loan");
        return;
    }

    // update balance and display for balance
    userAccount.bankBalance += amount;
    balanceDisplay.innerText = userAccount.bankBalance + " kr";
    userAccount.receivedLoan = true;
}

// adds salary to the bank
function addToBank() {
    // update users bank balance 
    userAccount.bankBalance += userAccount.outstandingPay;
    // update display of bank balance
    balanceDisplay.innerText = userAccount.bankBalance + " kr";
    // reset outstanding pay
    userAccount.outstandingPay = 0;
    // reset pay display
    payDisplay.innerText = "0 kr";
}

// increases work balance
function doWork() {
    // increase outstanding pay
    userAccount.outstandingPay += userAccount.salaryRate;
    // update display of pay
    payDisplay.innerText = userAccount.outstandingPay + " kr";
}

// takes info on selected lapotop and displays it
function displayLaptopInfo(event) {
    // get selected laptop
    let selectedLaptop = laptopList[dropdown.selectedIndex-1];
    // display features
    let featuresText = "";
    selectedLaptop.features.forEach(feat => {
        featuresText += feat + "\n";
    })
    laptopFeaturesParagraph.innerText = featuresText;
    // make buy button visible:
    buyButton.style.visibility = "visible";
    // display description image and price
    displayLaptopDescription(selectedLaptop);
}

// displays laptop description, price and image
function displayLaptopDescription(laptop) {
    let laptopImg = document.getElementById("laptop-img");
    laptopImg.setAttribute("src", laptop.imageurl);
    laptopImg.setAttribute("alt", "Laptop image");
    let descriptionSection = document.getElementById("laptop-description");
    descriptionSection.innerHTML = `<h1>${laptop.name}</h1>
        <p>${laptop.description}</p>`;
    let laptopPrice = document.getElementById("laptop-price-header");
    laptopPrice.innerText = laptop.price;

}

// checks if enough balance to buy a laptop. if yes takes money from the bank
// and displays message
function buyLaptop() {
    // check if sufficient funds
    let laptop = laptopList[dropdown.selectedIndex-1];
    let price = laptop.price;
    if (userAccount.bankBalance < price) {
        alert("You do not have a high enough bank balance to get this laptop");
        return;
    }
    // update bank balance
    userAccount.bankBalance -= price;
    balanceDisplay.innerText = userAccount.bankBalance + " kr";
    // reset received loan
    userAccount.receivedLoan = false; 
    // display that purchase went through
    alert(`Congratulations you have just bought the laptop: ${laptop.name}`);
}